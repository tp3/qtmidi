HEADERS += \
    plugins/winmm/qwinmmmidiplugin.h \
    plugins/winmm/qwinmmmidiinput.h \
    plugins/winmm/qwinmmmidioutput.h \
    plugins/winmm/qwinmmmidiinputbackend.h \
    plugins/winmm/qwinmmmidioutputbackend.h \
    plugins/winmm/qwinmmmidibackend.h

SOURCES += \
    plugins/winmm/qwinmmmidiplugin.cpp \
    plugins/winmm/qwinmmmidiinput.cpp \
    plugins/winmm/qwinmmmidioutput.cpp \
    plugins/winmm/qwinmmmidiinputbackend.cpp \
    plugins/winmm/qwinmmmidioutputbackend.cpp \
    plugins/winmm/qwinmmmidibackend.cpp

