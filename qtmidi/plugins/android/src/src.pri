HEADERS += \
    plugins/android/src/qandroidmidiplugin.h \
    plugins/android/src/qandroidmidinativewrapper.h \
    plugins/android/src/qandroidmidiinput.h \
    plugins/android/src/qandroidmidioutput.h

SOURCES += \
    plugins/android/src/qandroidmidiplugin.cpp \
    plugins/android/src/qandroidmidinativewrapper.cpp \
    plugins/android/src/qandroidmidiinput.cpp \
    plugins/android/src/qandroidmidioutput.cpp
