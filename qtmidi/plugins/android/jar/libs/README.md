# Dependencies

## Midi-Driver

See https://github.com/kshoji/USB-MIDI-Driver/releases/tag/v0.1.12.

The jar is extracted from the aar.

Jar is required for compiling the QtMidi wrapper, aar for bundling within the app.
